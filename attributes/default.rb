


syslog_ng_source 'my_appli' do
 index "02"
 host  "127.0.0.0"
 port  "514"
end

syslog_ng_file 'my_appli_file' do 
 index "02"
 source_name "my_appli"
 days_uncompressed "7"
 log_base "/var/log/"
 log_name "default.rb"
end

syslog_ng_filter 'my_appli_filter' do 
 index "04"
 filter "level(warning)"
end
